#Preparation for ChIP-seq pipeline

This document serves as a guide to how to design a PF pipeline from scratch. Pipeline developer must do an extensive litterature work before any implementation in order to be knowledgable about every single detail of the pipeline and to make sure we are implementing reproducible and robust analysis workflow.

In this document I am gathering important litterature about the subject (here ChIP-seq pipeline). Resources can be different, research papers, blueprints, presentations, etc .. everything that makes the global picture clearer is to be considered.

In order to develop a pipeline, developer should be aware of the subject itself, the biology behind the problem asked and the state of the art of the approaches already published to solve that problem. This is typically the work one can do before writing a review paper.

Pipeline development is a 3-steps work :

- Reading papers
- Sketching the pipeline
- Implementing the pipeline


# Reading

## ChIP-Seq pipelines design : interesting papers   

[Practical Guidelines for the Comprehensive Analysis of ChIP-seq Data](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3828144/)  
*This paper comes with a self explanatory diagram that is very useful*  

[A computational pipeline for comparative ChIP-seq analyses](http://www.ncbi.nlm.nih.gov/pubmed/22179591)  
*Mainly recipies in shell scripts but useful description of the process, this is interesting if we want to develop ou own seeds in python*  

[A short survey of computational analysis methods in analysing ChIP-seq data](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3525234/)  
*Nice listing of different approaches for peak calling*  

[Analyzing ChIP-seq data: preprocessing, normalization, differential identification, and binding pattern characterization](http://link.springer.com/protocol/10.1007%2F978-1-61779-400-1_18)  
*More insights into modeling enrichments and implementing statistics* 

[High-resolution mapping and characterization of open chromatin across the genome](http://www.ncbi.nlm.nih.gov/pubmed/18243105) 
*For deepseq experiments applies to DNAse-Seq and Chipseq*

[Computation for ChIP-seq and RNA-seq studies](http://www.nature.com/nmeth/journal/v6/n11s/full/nmeth.1371.html)  
*General description of the rational behind ChIP-seq and RNA-seq*

[ChIP-seq guidelines and practices of the ENCODE and modENCODE consortia](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3431496/)  
*Guidelines from ENCODE Consortium, good tips for the experimental side as well as the analytical one*

[HiChIP: a high-throughput pipeline for integrative analysis of ChIP-Seq data](http://www.biomedcentral.com/1471-2105/15/280)  
*Excellent paper generalizing all the aspects to consider before developing a chipseq pipeline*   


## ChIP-Seq pipelines design : Presentations / Slides

[Bioconductor chipseq package](http://www.bioconductor.org/packages/release/bioc/vignettes/chipseq/inst/doc/Workflow.pdf)

[Using Bioconductor for ChIP-seq experiments, (course 2008)](http://master.bioconductor.org/help/course-materials/2008/SeattleNov08/)

[ChIP-seq data analysis](http://biow.sb-roscoff.fr/ecole_bioinfo/training_material/chip-seq/documents/presentation_chipseq.pdf)
*Very useful with lots of figures*  

[Irreproducibility Discovery Rate IDR](https://sites.google.com/site/anshulkundaje/projects/idr)  
*Nice walk through implementation of the IDR*

## ChIP-Seq pipelines design : Software packages / Public Repos

### R/Bioconductor

[ChiPpeakAnno](http://bioconductor.org/packages/release/bioc/html/ChIPpeakAnno.html)

[BayesPeak](http://bioconductor.org/packages/release/bioc/html/ChIPpeakAnno.html)

[GenomicFeatures](http://www.bioconductor.org/packages/release/bioc/html/GenomicFeatures.html)

[SHortRead](http://www.bioconductor.org/packages/release/bioc/html/ShortRead.html)

[rtracklayer](http://www.bioconductor.org/packages/release/bioc/html/rtracklayer.html)

[seqLogo](http://www.bioconductor.org/packages/release/bioc/html/seqLogo.html)  
[DESeq](http://www.bioconductor.org/packages/release/bioc/html/DESeq.html)

### Binaries

[FASTX-toolkit : fastx_quality_stats]()  

[FASTX-toolkit : fastx_nucleotide_distribution_graph]()  

[Bowtie2]()  

[Samtools]()   

[FastQC]()  

[bedtools2]()  

[UCSCtools]()

[MACS]()

### Repos

[DeepTools]( https://github.com/fidelram/deepTools )  
*This is an excellent ressource, not only because of the tools it provides but also to learn more about metrics and chip-seq protocol* 


#Sketching the pipeline

According to the documents mentioned above, one possible implementation of the pipeline could be as shown below in this diagram. Such representation help in delimiting the tasks to be done, in order to implement efficiently

![chipseq_sketch]( https://bytebucket.org/radaniba/chipseq_pipeline_from_zero_to_expert/raw/0efd270b93a6ecb0398292a1d834665a1c2aa1d1/chipseq_sketch.png/chipseq_sketch.png.001.png?token=d1cdbaf1be6fa9754eb2cae7b23e4ef7ce17afaa )

# Implementing the pipeline

<div class="twitter-tweet" lang="en"><p lang="en" dir="ltr">A <a href="https://twitter.com/hashtag/data?src=hash">#data</a> analysis pipeline is always subjective...always !</p>&mdash; Radhouane Aniba (@radaniba) <a href="https://twitter.com/radaniba/status/631564210275680256">August 12, 2015</a></div>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> 

**for an example please refer to the `pipeline.sh` script** This is a reconstruction from a paper published in Nature. The only positive side of it is that it highlights some basic steps to process ChIP data, a lot of features are not included unfortunately (for this papers' authors and readers)

###Data Preprocessing

This is a QC step for the fastq (raw) data , we need to gather some **pre-alignment** statistics  to have an idea on sequencing quality as well as the size of the samples being used etc ...

We can do that using different approaches and methods, there are plenty of methods out there for doing this, we should think about the best way in terms of reproducility, support and efficiency (computing)

One option would be : 

- Step 1 :  Sequence Quality Check using FASTX-toolkit
- Step 2 :  Raw Read Counts

But after trying fastx-toolkit I was disappointed by the 'usability' and the quality of the ouptut. I tested FastQC in command line and it is much more efficient and practical. So another option would be 

- Step 1 - 2 : Running Fastqc on the raw data

This generates a directory with all the figures we need in one simple step (I developed a component for that)

*****
**BREAKPOINT** 

I highly recommend adding a breakpoint here. We need to take a look at the data quality to see if we need further cleaning / trimming / clipping ... Besides FastQC gives us useful information about the data to optimize / adjust parameters inside the following tasks of pipeline itself
*****

###Read mapping and visualization

This is a standard alignment procedure as we are used to do in the lab. That said we may want to add extra features to it in terms of **post-alignment** quality check

- Step 3 :  Read length check (truncate reads longer than threshold)
- Step 4 :  Read Mapping

We can borrow some of the plots used in Targeted Sequencing Analytics Suite to plot mapping qualities as well as unmapped/mapped reads

*****
**BREAKPOINT**

Horizontal look at our data : we need to loop over our aligned samples as a first screening of the alignment quality 
***** 

- Step 5 :  Plot unmapped/mapped reads fractions
- Step 6 : Plot Mapping qualities 

###Bias Analysis

To that we need to examine if our alignment have bias or not, example GC bias

- Step 7 : Compute GC Bias
- Step 8 : Correct GC Bias
- Step 9 : Assess ChIP strength ("did my ChIP work?")
- Step 10 :  Read Density visualization


*****
**BREAKPOINT**


At this stage we need to do a horizontal look at all our data so we need a way of looking at all our data at the same time, this why we need to dump all our outputs from last step in a single log that will be used in the following steps

***** 


###Assessing global reproducibility and similarity

After the last break point we take a look at all our gc corrected bam files, the idea is to try to correlate all-vs-all bam files in order to see if replicates across samples cluster together or not (we can use person correlation to create the heatmap)

- Step 11:   Assessing global reproducibility and similarity

###Peak calling and conservation analysis

- Step 12:   Peak Calling (MACS2)
- Step 12bis : Peak FDR thresholds table generator
- Step 13:   Peak Visualization
- Step 14:  Peak Conservation 

###Analysis of quantitative changes

- Step 15:  Define Enriched Region
- Step 16:  Data Normalization
- Step 17:  Quantitative change


## Pipeline seeds repos 

- Step 1-2 : FastQC
- Step 3 : Custom Python
- Step 4 : Bowtie2 + Samtools + (picard tools optional)
- Step 5 : Targeted Sequencing Analytics Suite (custom)
- Step 6 : Targeted Sequencing Analytics Suite (custom)
- Step 7 : computeGCbias
- Step 8 : correctGCbias
- Step 9 : bamFingerprint
- Step 10 : bamCoverage
- Step 11 : bamCompare
- Step 12 : MACS2 
- Step 12bis : Custom Python
- Step 13 : Custom Python
- Step 14 : Custom Python
- Step 15 : Custom Python
- Step 16 : Custom Python
- Step 17 : Custom Python

## PF components  

### Components already developed

- run_bowtie2
- run_samtools_sort
- run_samtools_index
- picard_rmdp

### Components to develop

- run_fastx_quality_stats (IO)
- run_fastx_boxplot_graph (G)
- run_fastx_nucleotide_distribution_graph (G)
(these three components are actually replaced with one single one FastQC)
- collect_outputs
- convert_bam_to_bed (IO)
- bam_to_stats (IO)
- create_read_density (IO)
- create_bigwig_files (IO, G)
- run_macs2 (IO)
- filter_macs2_peaks (IO)
- visualize_macs2_peaks (IO, G)
- calculate_enriched_reads (IO)
- run_computeGCbias
- run_correctGCbias
- run_bamCompare
- run_bamCoverage
- run_bamFingerprints

*IO : Input/Ouptput component type*  
*G : Graphic component type* 

## PF pipeline testing  

This is the first version of the ChIP-Seq pipeline devloped using the approach described above 

This Yaml file was generated using the components in dev branch

![chipseqpipeline](./chipseq.png)

### Test Input Data 

**SAMPLES**

ChIP-Seq pipeline requires data in this format below (*to be used with the pipeline runner*)

Example :

```
#sample_id  treatment_1 treatment_2 control_1   control_2   analysis_name
CX5461-RAD51-1_S8   /extscratch/shahlab/raniba/Tasks/component_dev/test_zone/APARICIO-233/fastq/CX5461-RAD51-1_S8_L001_R1_001.fastq.gz  /extscratch/shahlab/raniba/Tasks/component_dev/test_zone/APARICIO-233/fastq/CX5461-RAD51-1_S8_L001_R2_001.fastq.gz  /extscratch/shahlab/raniba/Tasks/component_dev/test_zone/APARICIO-233/fastq/Control-RAD51-1_S6_L001_R1_001.fastq.gz /extscratch/shahlab/raniba/Tasks/component_dev/test_zone/APARICIO-233/fastq/Control-RAD51-1_S6_L001_R2_001.fastq.gz RAD51-0vs10-7CX#1

```

Where we have 6 columns :

- sample_id : treatment sample id
- treatment_1 : forward fastq for the treatment
- treatment_2 : reverse fastq for the treatment
- control_1 : forward fastq for control/input
- control_2 : reverse fastq for control/input
- analysis_name : used for MACS2

**the pipeline will first run a quality check on the fastq files and then stops with a breakpoint, users are invited to manually check the quality of the data, pay attention to GC% and fragment lengths**

**SETUP**

The pipeline doesn't need specific setup there are just a few parameters you should add to the setup file and here is an example :

```

__OPTIONS__ factory_dir /genesis/extscratch/shahlab/pipelines/virtual_environments/targeted_sequencing_pipeline/lib/python2.
7/site-packages/pipeline_factory/
__OPTIONS__ components_dir  /genesis/extscratch/shahlab/raniba/Tasks/component_dev/components/
__OPTIONS__ drmaa_library_path  /opt/sge/lib/lx24-amd64/libdrmaa.so
__GENERAL__ python  /genesis/extscratch/shahlab/pipelines/virtual_environments/targeted_sequencing_pipeline/bin/python
__GENERAL__ samtools    /genesis/extscratch/shahlab/pipelines/apps/samtools-0.1.18/samtools
__GENERAL__ bowtie2 /genesis/extscratch/shahlab/pipelines/apps/bowtie2-2.0.2/bowtie2
__SHARED__  ref_genome  /genesis/extscratch/shahlab/pipelines/apps/bowtie2-2.0.2/genome/hg19_rDNA
__SHARED__  ref_genome_fa   /genesis/extscratch/shahlab/pipelines/apps/reference/GRCh37-lite.fa
__SHARED__  genome_2bits    /genesis/extscratch/shahlab/pipelines/apps/reference/hg19.2bit
__SHARED__  fragmentlength  250
__SHARED__  effectivegenomesize 3114492550

```

You can download the 2bits version of the genome from here  http://hgdownload.cse.ucsc.edu/gbdb/ Search for the .2bit ending. Otherwise, fasta files can be  converted to 2bit using the UCSC programm called faToTwoBit available for different plattforms at http://hgdownload.cse.ucsc.edu/admin/exe/


### Testing the pipeline

Test are being done on genesis currently . will update soon

### Results 

Expected results

- Fastqc reports and figures
- Alignments in bam format
- Alignments corrected for GC bias
- Density visualization (bigWig and bedGraph files)
- Bam coverage plots
- Bam fingerprints plots
- Reproducility plots (corelation heatmaps)
- MACS peaks

Further analysis will be added to clean up the peaks

## PF pipeline production pipeline

(under construction)
